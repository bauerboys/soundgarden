Template.home.events({
  'submit #startForm': function(event, tmpl) {
    var person = {
      firstName: tmpl.$('#firstName').val(),
      lastName: tmpl.$('#lastName').val(),
      email: tmpl.$('#email').val()
    };
    People.insert(person);
  }
});
