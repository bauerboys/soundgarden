People.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  }
});

People.deny({
  insert: function (userId, docs) {
    return false;
  },
  update: function (userId, docs, fields, modifier) {
    return false;
  },
  remove: function (userId, doc) {
    return false;
  }
});
